Name:           anaconda-extras-unity
Version:        0.4
Release:        1%{?dist}
Summary:        Unity-Linux customisation for the anaconda

Group:          Applications/System
License:        GPLv3
URL:            https://gitlab.com/unity-linux/anaconda-extras-unity
Source0:        %{name}-%{version}.tar.xz

Requires:       anaconda

%description
%{summary}.

%prep
%setup -q

%install
mkdir -p %{buildroot}/etc/anaconda/product.d/
install -m 0644 unity-workstation.conf %{buildroot}/etc/anaconda/product.d/

%clean
rm -rf %{buildroot}

%files
%config /etc/anaconda/product.d/unity-workstation.conf

%changelog
* Mon Apr 29 2019 JMiahMan <JMiahMan@unity-linux.org> - 0.4-1
- InstallClasses are no longer supported by Anaconda move to conf file setup

* Wed Jan 02 2019 JMiahMan <JMiahMan@unity-linux.org> - 0.3-2
- Disable Network on Boot not supported yet.
